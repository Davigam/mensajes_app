/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensaje_app;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

/**
 *
 * @author Isaac
 */
public class Inicio {
    public static void main(String[] args) throws SQLException {
        Conexion conexion = new Conexion();
        
        Scanner sc = new Scanner(System.in);
        
        int opcion = 0;
        
        do{
            System.out.println("---------------------------------");
            System.out.println("Aplicacion de mensajes");
            System.out.println("1. Crear mensaje");
            System.out.println("2. Listar mensaje");
            System.out.println("3. Editar mensaje");
            System.out.println("4. Eliminar Mensaje");
            System.out.println("5. Salir");
            
            opcion = sc.nextInt();
            
            switch (opcion){
                case 1:
                    ServicioDeMensaje.crearMensaje();
                    break;
                case 2:
                    ServicioDeMensaje.listarMensaje();
                    break;
                case 3: 
                    ServicioDeMensaje.editarMnesaje();
                    break;
                case 4:
                    ServicioDeMensaje.borrarMensaje();
                    break;
                case 5:
                    System.out.println("Has salido con exito, ten un gran dia");
                    break;
            }
        }while(opcion != 5);
    }
}

//crear listar editar eliminar
